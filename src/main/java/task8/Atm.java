package task8;
//Task8
public class Atm {
    private double exchangeRateDollarToRuble;
    private double exchangeRateRubleToDollar;

    private static int instanceCount=0;

    public Atm(double exchangeRateDollarToRuble, double exchangeRateRubleToDollar) {
        this.exchangeRateDollarToRuble = exchangeRateDollarToRuble;
        this.exchangeRateRubleToDollar = exchangeRateRubleToDollar;
        ++instanceCount;
    }

    public double exchangeDollarToRuble(double dollar){
        return (int)(dollar*exchangeRateDollarToRuble*100)/100.0;
    }

    public double exchangeRubleToDollar(double ruble){
        return (int)(ruble/exchangeRateDollarToRuble*100)/100.0;
    }

    public static int getInstanceCount() {
        return instanceCount;
    }

}
