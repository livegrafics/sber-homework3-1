package task8;
//Task8
public class Task8 {
    public static void main(String[] args) {
        System.out.println("ПАО Сбербанк");
        double exchangeDollarToRubRate = 58.25;
        double exchangeRubToDollarRate = 0.25;
        Atm atmSber = new Atm(58.25, 0.25);
        double summInRubles = 1000;
        double summInDollars = 1000;
        System.out.println("Курс рубля -> " + exchangeRubToDollarRate + " долларов за 1 рубль");
        System.out.println("Курс доллара -> " + exchangeDollarToRubRate + " рублей за 1 доллар");
        System.out.println(summInDollars + " Долларов = " + atmSber.exchangeDollarToRuble(summInDollars) + " рублей");
        System.out.println(summInRubles + " Рублей = " + atmSber.exchangeRubleToDollar(summInRubles) + " долларов");

        System.out.println("======================");

        System.out.println("АО БКС Банк");
        exchangeDollarToRubRate = 55.25;
        exchangeRubToDollarRate = 0.17;
        Atm atmBKS = new Atm(exchangeDollarToRubRate, exchangeRubToDollarRate);
        summInRubles = 1000;
        summInDollars = 1000;
        System.out.println("Курс рубля -> " + exchangeRubToDollarRate + " долларов за 1 рубль");
        System.out.println("Курс доллара -> " + exchangeDollarToRubRate + " рублей за 1 доллар");
        System.out.println(summInDollars + " Долларов = " + atmBKS.exchangeDollarToRuble(summInDollars) + " рублей");
        System.out.println(summInRubles + " Рублей = " + atmBKS.exchangeRubleToDollar(summInRubles) + " долларов");

        System.out.println("======================");

        System.out.println("Всего банкоматов: " + Atm.getInstanceCount());
    }
}
