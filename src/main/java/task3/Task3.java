package task3;
// Task3

import java.util.Arrays;

public class Task3 {
    public static void main(String[] args) {
        Student[] students = new Student[4];
        students[0] = new Student();
        students[1] = new Student();
        students[2] = new Student();
        students[3] = new Student();

        students[0].setName("Яков");
        students[0].setSurname("Яковлев");
        students[0].setGrades(new int[]{5, 4, 5, 0, 0, 0, 0, 0, 0, 0});

        students[1].setName("Станислав");
        students[1].setSurname("Серов");
        students[1].setGrades(new int[]{5, 4, 5, 2, 0, 0, 0, 0, 0, 0});

        students[2].setName("Петр");
        students[2].setSurname("Петров");
        students[2].setGrades(new int[]{5, 4, 5, 3, 0, 0, 0, 0, 0, 0});

        students[3].setName("Максим");
        students[3].setSurname("Якан");
        students[3].setGrades(new int[]{5, 4, 0, 0, 0, 0, 0, 0, 0, 0});

        System.out.println("Исходный массив");
        System.out.println(Arrays.toString(students));
        System.out.println();

        StudentService.sortBySurname(students);

        System.out.println("Отсортированный массив");
        System.out.println(Arrays.toString(students));
        System.out.println();

        System.out.println("Лучший студент: " + StudentService.bestStudent(students));
    }
}
