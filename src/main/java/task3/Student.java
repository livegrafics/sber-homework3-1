package task3;
// Task3

import java.util.Arrays;

public class Student {
    private String name;
    private String surname;
    private int[] grades;

    private int countGrades;
    private final int MAX_AMOUNT_GRADES = 10;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int[] getGrades() {
        return grades;
    }

    public void setGrades(int[] grades) {
        this.grades = grades;
        for (int i = 0; i < grades.length; i++) {
            if (grades[i] > 0) {
                ++countGrades;
            }
        }
    }

    public void setNewGrade(int newGrade) {
        if (countGrades < MAX_AMOUNT_GRADES && newGrade > 0 && newGrade <= 5) {
            ++countGrades;
            grades[countGrades - 1] = newGrade;
        } else if (countGrades == MAX_AMOUNT_GRADES && newGrade > 0 && newGrade <= 5) {
            for (int i = 1; i < grades.length; i++) {
                grades[i - 1] = grades[i];
            }
            grades[MAX_AMOUNT_GRADES - 1] = newGrade;
        }
    }

    public double getAverageGround() {
        double average = 0;
        for (int i = 0; i < countGrades; i++) {
            average += grades[i];
        }
        if (countGrades > 0) {
            return (int) (average / countGrades * 100) / 100.0;
        } else return 0;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", grades=" + Arrays.toString(grades) +
                ", countGrades=" + countGrades +
                ", MAX_AMOUNT_GRADES=" + MAX_AMOUNT_GRADES +
                '}';
    }

//    @Override
//    public int compareTo(Student o) {
//        if (this.getSurname().equals(o.surname)){
//            return 0;
//        }else return this.getSurname().compareTo(o.getSurname());
//    }
}

