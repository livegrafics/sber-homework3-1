package task3;
// Task3

public class StudentService {
    public static Student bestStudent(Student[] students) {
        double maxAverageGrade = 0;
        int numberBestStudent = 0;
        for (int i = 0; i < students.length; i++) {
            if (maxAverageGrade < students[i].getAverageGround()) {
                maxAverageGrade = students[i].getAverageGround();
                numberBestStudent = i;
            }
        }
        return students[numberBestStudent];
    }

    public static void sortBySurname(Student[] students) {
        for (int i = 0; i < students.length - 1; i++) {
            for (int j = i + 1; j < students.length; j++) {
                if (students[i].getSurname().compareTo(students[j].getSurname()) > 0) {
                    Student temp = students[i];
                    students[i] = students[j];
                    students[j] = temp;
                }
            }
        }
    }
}
