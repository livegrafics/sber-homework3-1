package task2;
//Task2

import java.util.Arrays;
import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Student student = new Student();
        student.setName("name1");
        student.setSurname("surname1");
        Scanner scanner = new Scanner(System.in);
        student.setGrades(new int[]{5, 4, 5, -1, -1, -1, -1, -1, -1, -1});
        int newGrade;
        do {
            System.out.println(student.getName() + " " + student.getSurname());
            System.out.println(Arrays.toString(student.getGrades()));
            System.out.println(student.getAverageGround());
            System.out.println();
            System.out.println("Введите оценку или -1 для окончания");
            newGrade = scanner.nextInt();
            student.setNewGrade(newGrade);
        } while (newGrade != -1);
    }
}
