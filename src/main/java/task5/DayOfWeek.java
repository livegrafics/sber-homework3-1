package task5;
//Task5
public class DayOfWeek {
    private byte numberDay;
    private String nameDay;

    public DayOfWeek(byte numberDay, String nameDay) {
        this.numberDay = numberDay;
        this.nameDay = nameDay;
    }

    @Override
    public String toString() {
        return numberDay + " " + nameDay;
    }
}
