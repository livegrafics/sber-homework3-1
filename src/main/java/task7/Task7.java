package task7;
//Task7
public class Task7 {
    public static void main(String[] args) {
        double side1 = 11;
        double side2 = 12;
        double side3 = 13;
        System.out.println(TriangleChecker.isRightTriangle(side1,side2,side3));

        side1 = 1;
        side2 = 2;
        side3 = 3;
        System.out.println(TriangleChecker.isRightTriangle(side1,side2,side3));
    }
}
