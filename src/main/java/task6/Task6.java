package task6;
//Task6
public class Task6 {
    public static void main(String[] args) {
        AmazingString amazingString=new AmazingString(new char[]{' ',' ','N','a','t','a','s','h','a',' ','k','u','k','u'});
        amazingString.outputAmazingString();
        amazingString.deleteLeadingSpaces();
        amazingString.outputAmazingString();
        System.out.println(amazingString.isOccuredSubstringAmazingString("nata"));
        System.out.println(amazingString.isOccuredSubstringAmazingString(new char[]{'t','a','s','h','a'}));
        System.out.println(amazingString.getLengthAmazingString());
        System.out.println(amazingString.getSymbolById(5));
        amazingString.reverseAmazingString();
        amazingString.outputAmazingString();
    }
}
