package task6;
//Task6
public class AmazingString {
    char[] amazingString;

    public AmazingString(char[] stringInChar) {
        this.amazingString = stringInChar;
    }

    public AmazingString(String string) {
        this.amazingString = convertStringToCharArray(string);
    }

    public char getSymbolById(int i) {
        return this.amazingString[i];
    }

    public int getLengthAmazingString() {
        int length=0;
        for (char s:amazingString){
            ++length;
        }
        return length;
    }

    public void outputAmazingString() {
        for (int i = 0; i < amazingString.length; i++) {
            System.out.print(amazingString[i]);
        }
        System.out.println();
    }

    public boolean isOccuredSubstringAmazingString(char[] subString) {
        boolean isMatches = false;
        for (int i = 0; i <= amazingString.length - subString.length; i++) {
            isMatches = false;
            if (amazingString[i] == subString[0]) {
                isMatches = true;
                for (int j = 1; j < subString.length; j++) {
                    if (amazingString[i + j] != subString[j]) {
                        isMatches = false;
                        break;
                    }
                }
            }
            if (isMatches) {
                break;
            }
        }
        return isMatches;
    }

    private char[] convertStringToCharArray(String string) {
        char[] charString = new char[string.length()];
        for (int i = 0; i < string.length(); i++) {
            charString[i] = string.charAt(i);
        }
        return charString;
    }

    public boolean isOccuredSubstringAmazingString(String substring) {
        return isOccuredSubstringAmazingString(convertStringToCharArray(substring));
    }

    public void deleteLeadingSpaces() {
        int posNotSpace = 0;
        for (int i = 0; i < amazingString.length; i++) {
            if (amazingString[i] != ' ') {
                posNotSpace = i;
                break;
            }
        }
        char[] tempString = new char[amazingString.length - posNotSpace];
        for (int i = 0; i < amazingString.length - posNotSpace; i++) {
            tempString[i] = amazingString[i + posNotSpace];
        }
        amazingString = tempString;
    }

    public void reverseAmazingString() {
        for (int i = 0; i < amazingString.length / 2; i++) {
            char temp = amazingString[i];
            amazingString[i] = amazingString[amazingString.length - i - 1];
            amazingString[amazingString.length - i - 1] = temp;
        }
    }
}
