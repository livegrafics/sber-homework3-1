package task1;

//Task1
import java.util.Random;

public class Cat {
    private void sleep() {
        System.out.println("Кот спит");
    }

    private void meow() {
        System.out.println("Кот говорит МЯУ");
    }

    private void eat() {
        System.out.println("Кот ест");
    }

    public void status() {
        Random random = new Random(System.currentTimeMillis());
        int status = random.nextInt(3) + 1;
        System.out.println("status=" + status);
        switch (status) {
            case 1:
                sleep();
                break;
            case 2:
                meow();
                break;
            case 3:
                eat();
                break;
        }
    }
}
