package task4;
//Task4
public class Task4 {
    public static void main(String[] args) {
        TimeUnit timeUnit = null;
        try {
            timeUnit = new TimeUnit(07, 01, 24);
            timeUnit.outputTimeIn24HoursFormat();
            timeUnit.outputTimeIn12HoursFormat();
        } catch (MyTimeException e) {
            System.out.println(e.getMessage());
        }

        System.out.println();

        if (timeUnit!=null){
            try {
                timeUnit.addToCurrentTime(23, 0, 0);
                timeUnit.outputTimeIn24HoursFormat();
                timeUnit.outputTimeIn12HoursFormat();
            } catch (MyTimeException e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
