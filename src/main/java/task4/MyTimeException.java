package task4;
// Task4
public class MyTimeException extends Exception {
    public MyTimeException(String message) {
        super(message);
    }
}
