package task4;
//Task4
public class TimeUnit {

    private final int MAX_HOURS_IN_DAYS = 24;
    private final int MAX_MINUTES_IN_HOURS = 60;
    private final int MAX_SECOND_IN_MINUTES = 60;

    private int hour;
    private int minute;
    private int second;

    public TimeUnit(int hour, int minute, int second) throws MyTimeException {
        if (hour < 24 && hour >= 0 && minute < 60 && minute >= 0 && second < 60 && second >= 0) {
            this.hour = hour;
            this.minute = minute;
            this.second = second;
        } else {
            throw new MyTimeException("Время задано некорректно");
        }
    }

    public TimeUnit(int hour, int minute) throws MyTimeException {
        this(hour,minute,0);
    }

    public TimeUnit(int hour) throws MyTimeException {
        this(hour,0,0);
    }

    public void outputTimeIn24HoursFormat() {
        System.out.printf("%02d:%02d:%02d%n",this.hour,this.minute,this.second);
    }

    public void outputTimeIn12HoursFormat() {
        boolean isSecondHalfOfDay = false;
        int hour12 = this.hour;
        if (hour12 > MAX_HOURS_IN_DAYS / 2) {
            isSecondHalfOfDay = true;
            hour12 -= (MAX_HOURS_IN_DAYS / 2);
        }
        System.out.printf("%02d:%02d:%02d",this.hour,this.minute,this.second);
        if (isSecondHalfOfDay) {
            System.out.println(" pm");
        } else System.out.println(" am");
    }

    public void addToCurrentTime(int hour, int minute, int second) throws MyTimeException {
        if (hour >= 0 && hour < MAX_HOURS_IN_DAYS && minute >= 0 && minute < MAX_MINUTES_IN_HOURS && second >= 0 && second < MAX_SECOND_IN_MINUTES) {
            this.second += second;
            if (this.second >= MAX_SECOND_IN_MINUTES) {
                ++this.minute;
                this.second -= MAX_SECOND_IN_MINUTES;
            }
            this.minute += minute;
            if (this.minute >= MAX_MINUTES_IN_HOURS) {
                ++this.hour;
                this.minute -= MAX_MINUTES_IN_HOURS;
            }
            this.hour += hour;
            if (this.hour >= MAX_HOURS_IN_DAYS) {
                this.hour -= MAX_HOURS_IN_DAYS;
            }
        } else {
            throw new MyTimeException("Некорректно задано время для изменения");
        }
    }
}
